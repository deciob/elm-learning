module HelpersTest exposing (..)

import ElmTest exposing (..)
import Date

import Helpers exposing (..)


tests : Test
tests =
    let
      startingTimestamp = 819159300000 -- 00:15:00
      hourAngle = getCurrentHourAngle
          <| Date.fromTime startingTimestamp
      hourAnglePlus59Seconds = getCurrentHourAngle
          -- after 59 seconds the hour hand does not move
          <| Date.fromTime (startingTimestamp + 1000 * 59)
      hourAnglePlus1Minute = getCurrentHourAngle
          -- but after 1 minute it does!
          <| Date.fromTime (startingTimestamp + 1000 * 60)
    in
      suite "Clock helper functions"
          [ test "getCurrentMinuteAngle 00:15:00"
              <| assertEqual (getCurrentMinuteAngle (Date.fromTime 819159300000)) (2 * pi)
          , test "getCurrentMinuteAngle 00:15:01"
              <| assertEqual (getCurrentMinuteAngle (Date.fromTime 819159301000)) (2 * pi + pi / 1800)
          , test "getCurrentHourAngle 00:15:01 and 00:15:03 have different angles"
              <| assertEqual hourAngle hourAnglePlus59Seconds
          , test "getCurrentHourAngle 00:15:01 and 00:15:03 have different angles"
              <| assertNotEqual hourAngle hourAnglePlus1Minute
          ]
