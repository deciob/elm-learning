module Main exposing (..)

import ElmTest exposing (..)
import HelpersTest


tests : Test
tests =
    HelpersTest.tests


main : Program Never
main =
    runSuite tests
