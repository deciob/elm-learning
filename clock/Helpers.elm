module Helpers exposing (..)

import Debug exposing (..)
import Date

-- HELPERS

getDelta : Float -> Float -> Float
getDelta count fraction =
    pi / fraction * count

getExactAngle : Float -> Float -> Float
getExactAngle count fraction =
    3 / 2 * pi + count * pi / fraction

getCurrentHourAngle : Date.Date -> Float
getCurrentHourAngle date =
    let
        currentMinute =
            toFloat (Date.minute date)

        currentHour =
            toFloat (Date.hour date)

        exactHourAngle =
            getExactAngle currentHour 6

        delta =
            getDelta currentMinute 360
        _ = Debug.log "exactHourAngle, delta" (currentMinute, currentHour, delta)
    in
        exactHourAngle + delta


getCurrentMinuteAngle : Date.Date -> Float
getCurrentMinuteAngle date =
    let
        currentSecond =
            toFloat (Date.second date)

        currentMinute =
            toFloat (Date.minute date)

        --_ = Debug.log "second, minute" (currentSecond, currentMinute)

        exactMinuteAngle =
            getExactAngle currentMinute 30

        delta =
            getDelta currentSecond 1800
    in
        exactMinuteAngle + delta


getCurrentSecondAngle : Date.Date -> Float
getCurrentSecondAngle date =
    let
        currentSecond =
            toFloat (Date.second date)

        exactSecondAngle =
            getExactAngle currentSecond 30
    in
        exactSecondAngle
