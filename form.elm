import Html exposing (..)
import Html.App as Html
import Html.Attributes exposing (..)
import Html.Events exposing (onInput, onClick)
import String exposing (..)
import Regex exposing (..)


main =
  Html.beginnerProgram { model = model, view = view, update = update }


-- MODEL

type alias Model =
  { name : String
  , age: String
  , password : String
  , passwordAgain : String
  , showErrors: Bool
  }


model : Model
model =
  Model "" "" "" "" False


-- UPDATE

type Msg
    = Name String
    | Age String
    | Password String
    | PasswordAgain String
    | ShowErrors


update : Msg -> Model -> Model
update msg model =
  case msg of
    Name name ->
      { model | name = name }

    Age age ->
      { model | age = age }

    Password password ->
      { model | password = password }

    PasswordAgain password ->
      { model | passwordAgain = password }

    ShowErrors ->
      { model | showErrors = if model.showErrors == False then True else False }




-- VIEW

view : Model -> Html Msg
view model =
  div []
    [ input [ type' "text", placeholder "Name", onInput Name ] []
    , input [ type' "text", placeholder "Age", onInput Age ] []
    , input [ type' "password", placeholder "Password", onInput Password ] []
    , input [ type' "password", placeholder "Re-enter Password", onInput PasswordAgain ] []
    , button [ onClick ShowErrors ] [ text "Submit" ]
    , viewValidation model
    ]

viewValidation : Model -> Html msg
viewValidation model =
  let
    (color, message) =
      if model.showErrors then
        if model.password /= model.passwordAgain then
          ("red", "Passwords do not match!")
        else if String.length(model.password) < 2 then
          ("red", "Password too short!")
        else if Regex.contains (regex "[A-Z]\\d") model.password == False then
          ("red", "Need caps and numbers!")
        else if Regex.contains (regex "\\d") model.age == False then
          ("red", "Age needs to be a number!")
        else
          ("green", "OK")
      else
        ("", "")
  in
    div [ style [("color", color)] ] [ text message ]
