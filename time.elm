module Main exposing (..)

import Debug exposing (..)
import Html exposing (Html, button, div)
import Html.App as App
import Html.Events exposing (onClick)
import Svg exposing (..)
import Svg.Attributes exposing (..)
import Time exposing (Time, second)
import Date


main =
    App.program
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }



-- MODEL


type alias Model =
    { time : Time
    --, initialTimeAngle: Float
    , paused : Bool
    }


init : ( Model, Cmd Msg )
init =
    ( Model 0 False, Cmd.none )



-- UPDATE


type Msg
    = Tick Time
    --| Now
    --| InitialTimeAngle
    | Paused


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Tick newTime ->
            ( { model | time = newTime }, Cmd.none )

        --Now newTime ->
        --    ( model
        --    , Date.now InitialTimeAngle
        --    )

        --InitialTimeAngle now ->
        --    let
        --      angel = 32.2
        --    in
        --      ( {model | initialTimeAngle = angel}
        --      , Cmd.none
        --      )

        Paused ->
            ( { model
                | paused =
                    if model.paused then
                        False
                    else
                        True
              }
            , Cmd.none
            )



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    if model.paused then
        Sub.none
    else
        Time.every second Tick



-- VIEW


view : Model -> Html Msg
view model =
    let
        secondsAngle =
            turns (Time.inMinutes model.time)

        minutesAngle =
            turns (Time.inHours model.time)

        currentMinute = toFloat (Date.minute (Date.fromTime model.time))
        currentMinuteAngle = currentMinute * pi / toFloat(30)

        _ = Debug.log "turns" Time.hour 
        --_ = Debug.log "turns" secondsAngle

        secHandX =
            toString (50 + 40 * cos secondsAngle)

        secHandY =
            toString (50 + 40 * sin secondsAngle)

        minHandX =
            toString (50 + 30 * cos minutesAngle)

        minHandY =
            toString (50 + 30 * sin minutesAngle)

        btnTitle =
            if model.paused then
                "Restart"
            else
                "Pause"
    in
        div []
            [ svg [ viewBox "0 0 100 100", width "300px" ]
                [ circle [ cx "50", cy "50", r "45", fill "#0B79CE" ] []
                , line [ x1 "50", y1 "50", x2 secHandX, y2 secHandY, stroke "#023963" ] []
                , line [ x1 "50", y1 "50", x2 minHandX, y2 minHandY, stroke "#023963" ] []
                ]
            , button [ onClick Paused ] [ text btnTitle ]
            ]
