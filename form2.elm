import Html exposing (..)
import Html.App as App
import Html.Attributes exposing (..)
import Html.Events exposing (onInput, onClick)
import Regex
import List
import String
import Debug exposing (..)


--Check that the password is longer than 8 characters.
--Make sure the password contains upper case, lower case, and numeric characters.
--Add an additional field for age and check that it is a number.
--Add a "Submit" button. Only show errors after it has been pressed.


main =
  App.beginnerProgram
    { model = model
    , view = view
    , update = update
    }




-- MODEL

type alias ValidationMsgs = List String


type alias Model =
  { name : String
  , password : String
  , passwordAgain : String
  , age: Int
  , isValid: Bool
  , validationMsgs: ValidationMsgs
  }


model : Model
model =
  Model "" "" "" 0 True []



-- UPDATE

doesPasswordMatch : Model -> ValidationMsgs -> ValidationMsgs
doesPasswordMatch model validationMsgs =
    let
      errorMsg = "Passwords do not match!"
    in
      if model.password == model.passwordAgain then
          validationMsgs
        else
          errorMsg :: validationMsgs


correctCharacters : Model -> ValidationMsgs -> ValidationMsgs
correctCharacters model validationMsgs =
    let
      errorMsg = "Password must contain strings, numbers, upper and lowercase characters!"
    in
      -- contains : Regex -> String -> Bool
      if Regex.contains (Regex.regex "[0-9]+[A-Z]+[a-z]+") model.password then
          validationMsgs
        else
          errorMsg :: validationMsgs


validate : Model -> ValidationMsgs
validate model =
    doesPasswordMatch model [] |> correctCharacters model


type Msg
    = Name String
    | Password String
    | PasswordAgain String
    | Validate


update : Msg -> Model -> Model
update msg model =
  case msg of
    Name name ->
      { model | name = name }

    Password password ->
      { model | password = password }

    PasswordAgain password ->
      { model | passwordAgain = password }

    Validate ->
      let
        validationMsgs = validate model
        _ = Debug.log "validationMsgs" (validationMsgs)
      in
        { model |
          isValid = (List.length validationMsgs) == 0,
          validationMsgs = validationMsgs
        }




-- VIEW


view : Model -> Html Msg
view model =
  div []
    [ input [ type' "text", placeholder "Name", onInput Name ] []
    , input [ type' "password", placeholder "Password", onInput Password ] []
    , input [ type' "password", placeholder "Re-enter Password", onInput PasswordAgain ] []
    , button [ onClick Validate ] [ text "Submit" ]
    , viewValidation model
    ]


viewValidation : Model -> Html msg
viewValidation model =
  let
    (color, messages) =
      if model.isValid then
        ("green", ["OK"])
      else
        ("red", messages)
  in
    div [ style [("color", color)] ] [ text (String.join ", " messages) ]
