module Main exposing (..)

import Debug exposing (..)
import Html exposing (Html, button, div)
import Html.App as App
import Html.Events exposing (onClick)
import Svg exposing (..)
import Svg.Attributes exposing (..)
import Time exposing (Time, second)
import Date
import Task


main =
    App.program
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }

-- MODEL


type alias Model =
    { time : Time
    , paused : Bool
    }


-- https://groups.google.com/d/msg/elm-discuss/bjUyCYR3Us4/PO8EMAaVBAAJ
init : ( Model, Cmd Msg )
init =
    ( Model 0 False
    , Time.now |> Task.perform (always (Now 0)) Now
    )



-- UPDATE


type Msg
    = Tick Time
    | Now Time
    | Paused


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Tick newTime ->
            ( { model | time = newTime }, Cmd.none )

        Now initialTime ->
            ( { model | time = initialTime }, Cmd.none )

        Paused ->
            ( { model
                | paused =
                    if model.paused then
                        False
                    else
                        True
              }
            , Cmd.none
            )



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    if model.paused then
        Sub.none
    else
        Time.every second Tick



-- VIEW


view : Model -> Html Msg
view model =
    let
        rd =
          90

        date =
            Date.fromTime model.time

        currentSecond =
            toFloat (Date.second date)

        currentSecondAngle =
            3 * pi / 2 + currentSecond * pi / 30

        currentMinuteAngle =
            getCurrentMinuteAngle date

        currentHourAngle =
            getCurrentHourAngle date

        secHandX =
            toString (rd + rd * 0.8 * cos currentSecondAngle)

        secHandY =
            toString (rd + rd * 0.8 * sin currentSecondAngle)

        minHandX =
            toString (rd + rd * 0.75 * cos currentMinuteAngle)

        minHandY =
            toString (rd + rd * 0.75 * sin currentMinuteAngle)

        hourHandX =
            toString (rd + rd * 0.55 * cos currentHourAngle)

        hourHandY =
            toString (rd + rd * 0.55 * sin currentHourAngle)

        btnTitle =
            if model.paused then
                "Restart"
            else
                "Pause"

        rds =
            toString rd
    in
        div []
            [ svg [ viewBox "0 0 200 200", width "900px" ]
                [ circle [ cx rds, cy rds, r (toString (rd * 0.9)), fill "#a6bddb" ] []
                , line [ x1 rds, y1 rds, x2 hourHandX, y2 hourHandY, stroke "#023963" ] []
                , line [ x1 rds, y1 rds, x2 minHandX, y2 minHandY, stroke "#023963" ] []
                , line [ x1 rds, y1 rds, x2 secHandX, y2 secHandY, stroke "#f03b20" ] []
                ]
            , button [ onClick Paused ] [ text btnTitle ]
            ]



-- HELPERS


getCurrentHourAngle : Date.Date -> Float
getCurrentHourAngle date =
    let
        currentMinute =
            toFloat (Date.minute date)

        currentHour =
            toFloat (Date.hour date)

        exactHourAngle =
            3 * pi / 2 + currentHour * pi / 6

        delta =
            2 * pi / 60 / 60 * currentMinute
    in
        exactHourAngle + delta


getCurrentMinuteAngle : Date.Date -> Float
getCurrentMinuteAngle date =
    let
        currentSecond =
            toFloat (Date.second date)

        currentMinute =
            toFloat (Date.minute date)

        exactMinuteAngle =
            3 * pi / 2 + currentMinute * pi / 30

        delta =
            2 * pi / 60 / 60 * currentSecond
    in
        exactMinuteAngle + delta
